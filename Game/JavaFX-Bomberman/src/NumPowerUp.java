import javafx.scene.image.ImageView;

public class NumPowerUp extends PowerUps {
	
	public NumPowerUp () {
		super();
	}

	public NumPowerUp (int xcoord, int ycoord, ImageView image) {
		super(xcoord, ycoord, image);
	}	

}