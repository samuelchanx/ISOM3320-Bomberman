public class LeaderBoard {
	private int [] score;
	private String [] name;

	public LeaderBoard () {
		score = new int[10];
		name = new String[10];
		this.update(5,6,"Samuel","Johannes");
        this.update(7,4,"Nick","Timture");
        this.update(6,5,"professor","somebody1");
	}

	public int[] getScore () {
		return score;
	}
	public String[] getName () {
		return name;
	}

	public void update (int score1, int score2, String name1, String name2) {
		for (int i = 0; i < score.length; i++) {
			if (score1 > score[i]) {
				for (int m = score.length-1;m>=i+1; m--) {
					score[m]= score[m-1];
					name[m] = name[m-1];
				}
				score[i] = score1;
				name[i] = name1;
				break;
			}
		}
		for (int j = 0; j < score.length; j++) {
			if (score2 > score[j]) {
				for (int n = score.length-1;n>=j+1; n--) {
					score[n]= score[n-1];
					name[n] = name[n-1];
				}
				score[j] = score2;
				name[j] = name2;
				break;
			}
		}
	}
}

	