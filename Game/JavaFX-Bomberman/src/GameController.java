import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Parent;


public class GameController implements Initializable {

    private static final Integer STARTTIME = 15;
    private static final Integer SIZE = 45;
    int[] removableBricksX = { 180, 270, 360, 450, 540, 90, 270, 450, 180, 270, 360, 450, 540,  90, 270, 450, 180, 270, 360, 450, 540,  90, 270, 450, 180, 270, 360, 450, 540};
    int[] removableBricksY = {  45,  45,  45,  45,  45,135, 135, 135, 225, 225, 225, 225, 225, 315, 315, 315, 405, 405, 405, 405, 405, 495, 495, 495, 585, 585, 585, 585, 585};
    
    // TODO: init speedPowerUpList and numPowerUpList using for loop and content of removableBrickList Array
    int[] speedPowerUpXList = { 180, 360, 540, 450,  90, 360, 540, 180, 360, 540 };
    int[] speedPowerUpYList = {  45,  45,  45, 225, 315, 405, 405, 585, 585, 585 };

    int[] numPowerUpXList = { 270, 450, 90, 450, 270, 450, 270, 450, 90, 450, 270, 450 };
    int[] numPowerUpYList = { 45,  45,  135,135, 225, 315, 405, 405, 495,495, 585, 585 };
    
    private Timeline timeline;
    private Label timerLabel = new Label();
    private Integer timeSeconds = STARTTIME;
    private Brick[][] brickList;
    private Bomb[][] bombList = new Bomb[15][15];
    private PowerUps[][] powerUpList;
    
    private GamePlayLoop gamePlayLoop;

    private LeaderBoard lb = new LeaderBoard();
    
    protected String firstPlayerDirection;
    protected String secondPlayerDirection;
    
    private Player player1;
    private Player player2;

    private Date player1PastWalkTime;
    private Date player2PastWalkTime;
    private Date currentTime;

    private static int interval;
    private static Timer timer;

    private URL url;
    private ResourceBundle rb;

    
    @FXML
    private Button quitButton;

    @FXML
    private Label timeLabel, firstPlayerScore, secondPlayerScore;
    
    @FXML
    private Pane gamePane;

    @FXML
    private FlowPane quitPane;
    
    // initialize all the elements on the screen
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.url = url;
        this.rb = rb;
        timeLabel.setText("1:30");
        gamePlayLoop = new GamePlayLoop(this);
        gamePlayLoop.start();
        

        initializeTimer();

        initializePowerUps();
        initializeFixedBrick();
        initializeRemovableBrick();
        initializePlayer();
        
        initializeLeaderBoard();

        quitButton.setText("Quit");
        quitButton.setOnAction(event -> {
            showLeaderBoard();
        });
        // For testing exist
        // boolean test = isPowerUpExist(180, 45) ;
        // System.out.printf("Power up exists at (%d, %d): %b \n", 180, 45, test);
        // test = powerUpList[180/45][1] instanceof SpeedPowerUp;
        // System.out.printf("Power up is instanceof SpeedPowerUp at (%d, %d): %b \n", 180, 45, test);
    }

    // give initial data to the leader board
    private void initializeLeaderBoard(){
        // lb.update(10,1,"A","L");
        // lb.update(9,2,"B","K");
        // lb.update(8,3,"C","J");
        // lb.update(5,6,"Samuel","Johannes");
        // lb.update(7,4,"Nick","Timture");
        // lb.update(6,5,"professor","somebody1");
        // int firstScore = player1.getscore();
        // int secondScore = player2.getscore();
        // lb.update(firstScore, secondScore, "Player1", "Player2");
    }
    
    // start the timer in the game screen, show the leader board when timer ends
    private void initializeTimer() {
        timer = new Timer();
        interval = 90;
        int minute = interval/60;
        int second = interval%60;

        System.out.println(minute+":"+second);
        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                Platform.runLater(new Runnable() { // need to use platform.runlater to update the UI in separate thread
                    public void run() {
                        int countdown = setInterval();
                        timeLabel.setText((countdown/60+":"+countdown%60));
                        if (countdown/60 == 0 && countdown%60 == 0){
                            showLeaderBoard();
                        }
                    }
                });
            }
        }, 1000, 1000);
    }

    // Print the scores and names of Leaderboard onto the gamePane, stop the game
    @FXML
    private void showLeaderBoard() {
        gamePlayLoop.stop();

        Scene scene = gamePane.getScene();

        scene.setOnKeyPressed((KeyEvent event) -> {});
        timer.cancel();

        quitButton.setText("Replay");

        quitButton.setOnAction(event -> {
            try {
                gamePane.getChildren().clear();
                initialize(url, rb);
                // //get reference to the button's stage
                // Stage stage = (Stage) quitButton.getScene().getWindow();
                // //load up game FXML document
                // Parent root = FXMLLoader.load(getClass().getResource("GameScreen.fxml"));
                // //create a new scene with root and set the stage
                // Scene newScene = new Scene(root);
                // stage.setScene(newScene);
                // stage.show();
            } catch (Exception e){
                System.out.println("cannot load game screen");
            }
            
        });

        gamePane.getChildren().clear();

        int firstScore = player1.getscore();
        int secondScore = player2.getscore();
        lb.update(firstScore, secondScore, "Player1", "Player2");

        String [] name = lb.getName();
        int [] score = lb.getScore();
        Label nameLabel = new Label();
        Label scoreLabel = new Label();
        nameLabel.setLayoutX(100);
        nameLabel.setLayoutY(10);
        nameLabel.setText("Name");
        scoreLabel.setLayoutX(200);
        scoreLabel.setLayoutY(10);
        scoreLabel.setText("Score");
        gamePane.getChildren().addAll(nameLabel, scoreLabel);

        for (int i=0; i<name.length; i++){
            System.out.println("name: "+name[i]);
        }

        for (int i=0; i<score.length && name[i] != null; i++){
            nameLabel = new Label();
            scoreLabel = new Label();
            nameLabel.setLayoutX(100);
            nameLabel.setLayoutY((i+1)*50);
            nameLabel.setText(name[i]);
            scoreLabel.setLayoutX(200);
            scoreLabel.setLayoutY((i+1)*50);
            scoreLabel.setText(score[i]+"");
            gamePane.getChildren().addAll(nameLabel, scoreLabel);
        }
    }

    // Helper function for initialize Timer
    private static final int setInterval() {
        if (interval == 1)
            timer.cancel();
        return --interval;
    }

    // Add Power ups into the right positions of gamePane
    private void initializePowerUps(){
        ImageView powerUpImage;

        powerUpList = new PowerUps[15][15];
        for (int i=0; i<speedPowerUpXList.length; i++){
            powerUpImage = new ImageView(new Image(getClass().getResourceAsStream("speedPowerUp.png")));

            int x = speedPowerUpXList[i];
            int y = speedPowerUpYList[i];

            powerUpImage.setLayoutX(x);
            powerUpImage.setLayoutY(y);
            powerUpList[x/SIZE][y/SIZE] = new SpeedPowerUp(x, y, powerUpImage);

            gamePane.getChildren().add(powerUpList[x/SIZE][y/SIZE].getImage());
        }

        for (int i=0; i<numPowerUpXList.length; i++){
            powerUpImage = new ImageView(new Image(getClass().getResourceAsStream("numPowerUp.png")));

            int x = numPowerUpXList[i];
            int y = numPowerUpYList[i];

            powerUpImage.setLayoutX(x);
            powerUpImage.setLayoutY(y);
            powerUpList[x/SIZE][y/SIZE] = new NumPowerUp(x, y, powerUpImage);

            gamePane.getChildren().add(powerUpList[x/SIZE][y/SIZE].getImage());
        }
    }

    // Add fixed bricks into the right positions of gamePane
    private void initializeFixedBrick(){
        ImageView brickImage;

        brickList = new Brick[15][15];
        for (int i=1; i<brickList.length-1; i += 2){
            for (int j=1; j<brickList.length-1; j+=2 ){
                brickImage = new ImageView(new Image(getClass().getResourceAsStream("brick.png")));

                int x = i * SIZE;
                int y = j * SIZE;

                brickImage.setLayoutX(x);
                brickImage.setLayoutY(y);
                brickList[i][j] = new Brick(x, y, brickImage);

                gamePane.getChildren().add(brickList[i][j].getImage());
            }
        }

    }

    // Add removable bricks into the right positions of gamePane
    private void initializeRemovableBrick() {
        ImageView brickImage;

        for (int i=0; i<removableBricksX.length; i++){
            brickImage = new ImageView(new Image(getClass().getResourceAsStream("removableBrick.png")));

            int x = removableBricksX[i];
            int y = removableBricksY[i];

            brickImage.setLayoutX(x);
            brickImage.setLayoutY(y);
            brickList[x/45][y/45] = new RemovableBrick(x, y, brickImage);

            gamePane.getChildren().add(brickList[x/45][y/45].getImage());
        }
    }

    // Add player 1 and 2 into right positions of gamePane
    private void initializePlayer() {
        ImageView playerImage;
        ImageView secondPlayerImage;

        firstPlayerScore.setText("0");
        secondPlayerScore.setText("0");


        try {
            playerImage = new ImageView(new Image(getClass().getResourceAsStream("bomberman.png")));
            player1 = new Player(0, 0, 1, 1, 1, 0, 1, playerImage, this);
        } catch (Exception ex){
            System.out.println("Player image file not found!");
        }

        try {
            secondPlayerImage = new ImageView(new Image(getClass().getResourceAsStream("bomberman2.png")));
            secondPlayerImage.setLayoutX(630);
            secondPlayerImage.setLayoutY(630);

            player2 = new Player(630, 630, 2, 1, 1, 0, 1, secondPlayerImage, this);

            gamePane.getChildren().addAll(player1.getImage(), player2.getImage());
        } catch (Exception ex){
            System.out.println("Player image file not found!");
        }
    }

    // update the player location, and handle add speedPowerUp, numPowerUp situation
    public void updatePlayerLocation(Player player){

        System.out.println("update player location");

        gamePane.getChildren().remove(player.getImage());
        ImageView playerImage = player.getImage();

        playerImage.setLayoutX(player.getxpos());
        playerImage.setLayoutY(player.getypos());

        player.setImage(playerImage);
        gamePane.getChildren().add(player.getImage());

        int small_x = player.getxpos()/SIZE;
        int small_y = player.getypos()/SIZE;

        if (isPowerUpExist(player.getxpos(), player.getypos()) ){
            if (powerUpList[small_x][small_y] instanceof SpeedPowerUp){
                player.updatespeed();
                System.out.println("updated speed");
            }
            if (powerUpList[small_x][small_y] instanceof NumPowerUp){
                player.updatebombcount();
                System.out.println("updated bombcount");
            }
            removePowerUp(player.getxpos(), player.getypos());
        }
    }

    // return true if there is a fixed brick in xpos, ypos
    public boolean isBrickExist (int xPos, int yPos){
        return ( brickList[xPos/SIZE][yPos/SIZE] != null ) ;
    }

    // return true if there is a power up in xpos, ypos
    public boolean isPowerUpExist (int xPos, int yPos){
        return ( powerUpList[xPos/SIZE][yPos/SIZE] != null ) ;
    }

    // Handle bomb explosion situation
    // x and y represents the range of explosion (excluded the case of bombing area out of pane already)
    // bombX and bombY represents the positions of the bomb
    public void handleExplode(ArrayList<Integer> x, ArrayList<Integer> y, int bombX, int bombY){
        checkRemoveBrick(x, y);
        checkPlayerDie(bombX, bombY);
        removeBomb(bombX, bombY);
    }
    
    // Check if there is a removable brick from the given arraylist
    // remove it if yes; do nothing otherwise
    public void checkRemoveBrick (ArrayList<Integer> x, ArrayList<Integer> y) {

        for(int j = 0; j<x.size(); j++) {

            if (brickList[x.get(j)/SIZE][y.get(j)/SIZE] != null )
            {
                if (brickList[x.get(j)/SIZE][y.get(j)/SIZE] instanceof RemovableBrick)
                {
                    gamePane.getChildren().remove(brickList[x.get(j)/SIZE][y.get(j)/SIZE].getImage());
                    brickList[x.get(j)/SIZE][y.get(j)/SIZE] = null;
                }
            }
        }
    }

    // Check if there is a player in the bombing area
    // add score to another player if there is one; do nothing otherwise
    public void checkPlayerDie(int bombX, int bombY){

        if ( (player1.getxpos() == bombX && player1.getypos() == bombY+45) ||
         (player1.getxpos() == bombX && player1.getypos() == bombY-45) ||
         (player1.getxpos() == bombX-45 && player1.getypos() == bombY) ||
         (player1.getxpos() == bombX+45 && player1.getypos() == bombY) || 
         (player1.getxpos() == bombX && player1.getypos() == bombY) ) 
        {
            System.out.println("Player 2 Updated score");
            player2.updatescore();
            secondPlayerScore.setText(player2.getscore()+"");
        }
        if ( ((player2.getxpos() == bombX) && (player2.getypos() == bombY+45) )||
         (player2.getxpos() == bombX && player2.getypos() == bombY-45) ||
         (player2.getxpos() == bombX-45 && player2.getypos() == bombY) ||
         (player2.getxpos() == bombX+45 && player2.getypos() == bombY) || 
         (player2.getxpos() == bombX && player2.getypos() == bombY) ) 
        {
            System.out.println("Player 1 Updated score");
            player1.updatescore();
            firstPlayerScore.setText(player1.getscore()+"");
        }
    }

    // Add a bomb into the gamePane of x, y coordinates
    public void addBomb(int x, int y){
        try {
            int small_x = x / SIZE; // small_x, small_y is the index of array of the corresponding x, y
            int small_y = y / SIZE;

            ImageView bombImage = new ImageView(new Image(getClass().getResourceAsStream("bomb.png")));
            bombImage.setLayoutX(x);
            bombImage.setLayoutY(y);

            bombList[x/SIZE][y/SIZE] = new Bomb(x, y, bombImage);
            gamePane.getChildren().add(bombList[small_x][small_y].getImage());
        } catch (Exception ex){
            System.out.println("Bomb image file not found!");
        }
    }

    // remove the bomb from gamePane
    public void removeBomb(int x, int y){
        gamePane.getChildren().remove( bombList[x/SIZE][y/SIZE].getImage() );
        bombList[x/SIZE][y/SIZE] = null;
        System.out.println("removed bomb");
    }

    // remove the power up from gamePane
    public void removePowerUp(int x, int y){
        gamePane.getChildren().remove( powerUpList[x/SIZE][y/SIZE].getImage() );
        powerUpList[x/SIZE][y/SIZE] = null;
        System.out.println("removed powerup");
    }

    // Handle Event on the scene
    public void sceneEventHandling() {

        Scene scene = gamePane.getScene();

        scene.setOnKeyPressed((KeyEvent event) -> {

            switch (event.getCode()) {
                case UP:    handleWalking("UP");    break;
                case DOWN:  handleWalking("DOWN");  break;
                case LEFT:  handleWalking("LEFT");  break;
                case RIGHT: handleWalking("RIGHT"); break;
                case ENTER: player1.walking("ENTER");break;
                case W: handleWalking("W"); break;
                case S: handleWalking("S"); break;
                case A: handleWalking("A"); break;
                case D: handleWalking("D"); break;
                case C: player2.walking("C"); break;
                default: break;
            }});

    }

    // handle directions walking by players, according to the speed property of player
    // allow walking only when the dedicated time has passed
    // walk for every 0.25/player.speed seconds
    // input should be one of UP|DOWN|RIGHT|LEFT OR W|S|A|D
    private void handleWalking(String walkDirection){
        currentTime = new Date();
        boolean isPlayerOne = true;
        double playerWalkDuration = 0;

    // decide whether it is player1 or player2
        if (walkDirection.matches("UP|DOWN|RIGHT|LEFT")){
            isPlayerOne = true;
        // player can only walk every 0.25/speed seconds
            playerWalkDuration = 0.25/player1.getspeed();
        }
        else if (walkDirection.matches("W|S|A|D")){
            isPlayerOne = false;
            playerWalkDuration = 0.25/player2.getspeed();
        }
        else
            System.out.println("error, should not have non direction string passed");
        
        if (isPlayerOne){
            // walked for once already
            if (player1PastWalkTime != null){
                double timeElapsed = ( currentTime.getTime() - player1PastWalkTime.getTime() ) / 1000.0;
                System.out.println("timeElapsed: "+timeElapsed);
                if ( timeElapsed >= playerWalkDuration){
                    player1.walking(walkDirection);
                    player1PastWalkTime = currentTime;
                }
            }
            else {
            // first time walking
                System.out.println("first time walking");
                player1.walking(walkDirection);
                player1PastWalkTime = currentTime;
            }

        }
        else { // handle player 2 situation
            if (player2PastWalkTime != null) // walked for once already
            { 
                double timeElapsed = ( currentTime.getTime() - player2PastWalkTime.getTime() ) / 1000.0;
                System.out.println("timeElapsed: "+timeElapsed);
                if ( timeElapsed >= playerWalkDuration)
                {
                 player2.walking(walkDirection);
                 player2PastWalkTime = currentTime;
             }
         }
         else {            
                // first time walking
            System.out.println("first time walking");
            player2.walking(walkDirection);
            player2PastWalkTime = currentTime;    
        }
    }
}
}


