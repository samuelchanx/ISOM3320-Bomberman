import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.fxml.FXMLLoader;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class Bomberman extends Application {
	private static final double WIDTH = 740, HEIGHT = 675;
	private Pane splashPane;
	private Scene scene;

	/* Game starts here */
	public void start(Stage primaryStage) throws Exception{
		primaryStage.setTitle( "Bomberman" );
		splashPane = FXMLLoader.load(getClass().getResource("SplashScreen.fxml"));

		scene = new Scene( splashPane, WIDTH, HEIGHT );
		
		primaryStage.setScene( scene );
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}