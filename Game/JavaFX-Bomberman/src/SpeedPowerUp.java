import javafx.scene.image.ImageView;

public class SpeedPowerUp extends PowerUps {
	
	public SpeedPowerUp () {
		super();
	}

	public SpeedPowerUp (int xcoord, int ycoord, ImageView image) {
		super(xcoord, ycoord, image);
	}	

	public void addSpeed (Player p) {
		p.setspeed(p.getspeed()+1);
	}

}